# CFFProxy

Proxy for CORS issues.

## Pre Install

```bash
curl https://get.volta.sh | bash
```

## Install

```bash
volta install node
volta install @cloudflare/wrangler
```

## Post Install

Modify variable in .toml file as follow `workers_dev = true`.

## Usage 

```bash
wrangler dev
```
Then send your requests to localhost:8787/[url].

---

Further documentation for Wrangler can be found [here](https://developers.cloudflare.com/workers/tooling/wrangler).
